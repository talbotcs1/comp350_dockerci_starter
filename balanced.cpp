
#include <stack>
#include <string>
#include <iostream>

// Returns true if the parenthesis,
//  square brackets, and curly braces
//  in text are balanced
bool isBalanced(const std::string &text){

  char c;
  char fromStack;
  std::stack<char> unmatched;
    // comment to get a Docker notification a second time

  for(int i=0; i<text.size(); i++){
    c = text[i];
    switch(c){
    case '[':
    case '{':
    case '(':
      unmatched.push(c);
      break;
    case ']':
      if(unmatched.empty() || unmatched.top() != '['){
        return false;
      }
      unmatched.pop();
      break;
    case '}':
      if(unmatched.empty() || unmatched.top() != '{'){
        return false;
      }
      unmatched.pop();
      break;
    case ')':
      if(unmatched.empty() || unmatched.top() != '('){
        return false;
      }
      unmatched.pop();
      break;

    }
  }
  
  return true;
}

bool runTest(const std::string &str, const bool correctAnswer){
    bool answer = isBalanced(str);
    if(answer != correctAnswer){
        std::cerr << "TEST FAILED on \"" << str << "\"" << std::endl;
        std::cerr << "        The answer should have been " << correctAnswer << " but was " << answer << std::endl;
        return false;
    }
    return true;
}

int main(){ 
    const char* inputs[] = {
            "This has ( parens ) in the right order but [ the match is } the wrong type",
            "no parens; should still be balanced",
            "The match for ( is \n on a different line ), and that is okay",
            "This test [ starts out okay ] but then there is this extra ) before I see the (, which is [[ not ]] balanced",
            "start with some text [ then some more ( foobar ) again { some more nesting } but there is no closing square bracket here",
            "(This file has balanced parenthesis)\n(Even though some are { nested },\nthey are [ still [ balanced ] ]\nand even though they span different lines, that shouldn't matter )"
    };
    
    const bool answers[] = {false, true, true, false, false, true};
    
    const int numTests = sizeof(answers) / sizeof(bool);
    
    
    for(int i=0; i<numTests; i++){
        bool passed = runTest(inputs[i], answers[i]);
        if(!passed){
            return 1;
        }
    }
    
    // 0 exit code indicates success
    return 0;
}


